/**
   sơ đồ đấu chân
  gpio 16->D7 quạt,
  gpio 13->D0 máy bơm,
  gpio 14->D5 dht11,
  gpio 12->D6 servo,
  D1/D2(light) I2C,
*/

#include <Wire.h>         // thư viện giao tiếp I2C
#include <BH1750.h>       // lightMeter library
BH1750 lightMeter;

// servo library
#include <Servo.h>
#define SERVOPIN 12     // GPIO12 -> D6 
#define SERVO_dong 0
#define SERVO_mo 140

#define DHTPIN 14 // GPIO14 -> D5
#define DHTTYPE DHT11
Servo servo_1;

#include "DHT.h" // dht11 library


// DHT type define

DHT dht(DHTPIN, DHTTYPE);
// esp
#include <ESP8266WiFi.h>
#include "Thing.CoAP.h"
Thing::CoAP::Server server;
Thing::CoAP::ESP::UDPPacketProvider udpProvider; //packet structure for a ESP secured UDP packet in Transport Mode
//Change here your WiFi settings
char* ssid = "main";
char* password = "publicvoidmain";
#define MAY_BOM 13 //D7
#define QUAT 16 //D0
// temp
// Pivoting Optical Servo = pos
// value x10 ~ real_value. Nghĩa là real_value sẽ đọc giá trị độ ẩm đất 10 lần
int value, real_value, pos = 0, action = 1;
void setup() {
  pinMode(MAY_BOM, OUTPUT);
  pinMode(QUAT, OUTPUT);
  digitalWrite(MAY_BOM, HIGH);
  digitalWrite(QUAT, HIGH);
  Serial.begin(115200);
  Serial.println(F("Initializing"));
  // lightMeter
  Wire.begin();
  lightMeter.begin();
  Serial.println(F("lightMeter begin"));
  // dht begin
  dht.begin();
  Serial.println(F("dht11 begin"));
  //servo
  servo_1.attach(SERVOPIN);
  servo_1.write(SERVO_dong);
  //Try and wait until a connection to WiFi was made
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println(F("Connecting to WiFi.."));
  }
  Serial.println(F("Connected to the WiFi network"));
  Serial.println(F("My IP: "));
  Serial.println(WiFi.localIP());
  //Configure our server to use our packet handler (It will use UDP)
  server.SetPacketProvider(udpProvider);
  //Create read infomation
  server.CreateResource("Readinfomation", Thing::CoAP::ContentFormat::TextPlain, false)
  .OnGet([](Thing::CoAP::Request & request) {
    Serial.println(F("GET Request all sensor'"));
    std::string result;
    // đọc cảm biến đất
    for (int i = 0; i < 10; i++) {
      real_value += analogRead(A0);
    }
    value = real_value / 10;
    real_value = 0;
    int percent = map(value, 100, 1023, 0, 100);
    percent = 100 - percent;
    Serial.print(percent);
    Serial.print('%');
    result += "độ ẩm đất: ";
    String per = String(percent);
    result += std::string(per.c_str());
    result += "%\n";
    Serial.print(F("Giá Trị Analog: "));
    Serial.print(value);
    Serial.println();
    // đọc cảm biến ánh sáng
    float lux = lightMeter.readLightLevel();
    Serial.print(F("Light: "));
    Serial.print(lux);
    Serial.println(" lx");
    result += "Cường độ ánh sáng: ";
    String luxs = String(lux);
    result += std::string(luxs.c_str());
    result += "lx\n";
    // đọc độ ẩm, nhiệt độ không khí
    float h = dht.readHumidity();
    float t = dht.readTemperature();
    // reads failed and exit early (to try again).
    if (isnan(h) || isnan(t)) {
      Serial.println(F("Failed to read from DHT sensor!"));
    }
    Serial.print(F("Humidity: "));
    Serial.print(h);
    Serial.print(" %\t");
    Serial.print(F("Temperature: "));
    Serial.print(t);
    Serial.println(F(" *C \n"));
    //
    result += "Nhiệt độ: ";
    String ts = String(t);
    result += std::string(ts.c_str());
    result += "*C\n";
    //
    result += "Độ ẩm không khí: ";
    String hs = String(h);
    result += std::string(hs.c_str());
    result += "%\n";
    return Thing::CoAP::Status::Content(result);
  });
  // end of ...
  server.CreateResource("MayBom", Thing::CoAP::ContentFormat::TextPlain, true)
  .OnGet([](Thing::CoAP::Request & request) {
    std::string result;
    if (digitalRead(MAY_BOM) == LOW)
      result = "Đang bơm";
    else
      result = "Không sử dụng";
    return Thing::CoAP::Status::Content(result);
  }).OnPost([](Thing::CoAP::Request & request) {
    auto payload = request.GetPayload();
    std::string message(payload.begin(), payload.end());
    if (action == 1) {
      if (message == "on") {
        digitalWrite(MAY_BOM, LOW);
      } else if (message == "off") {
        digitalWrite(MAY_BOM, HIGH);
      } else {
        return Thing::CoAP::Status::BadRequest();
      }
      if (message == "on") {
        return Thing::CoAP::Status::Created("Đã bật máy bơm !");
      } else {
        return Thing::CoAP::Status::Created("Đã tắt máy bơm !");
      }
    } else {
      return Thing::CoAP::Status::Created("Đang bật chế độ auto!");
    }
  });
  server.CreateResource("Quat", Thing::CoAP::ContentFormat::TextPlain, true)
  .OnGet([](Thing::CoAP::Request & request) {
    std::string result;
    if (digitalRead(QUAT) == LOW)
      result = "Đang sử dụng";
    else
      result = "Không sử dụng";
    return Thing::CoAP::Status::Content(result);
  }).OnPost([](Thing::CoAP::Request & request) {
    auto payload = request.GetPayload();
    std::string message(payload.begin(), payload.end());
    if (action == 1) {
      if (message == "on") {
        digitalWrite(QUAT, LOW);
      } else if (message == "off") {
        digitalWrite(QUAT, HIGH);
      } else {
        return Thing::CoAP::Status::BadRequest();
      }
      if (message == "on") {
        return Thing::CoAP::Status::Created("Đã bật quạt !");
      } else {
        return Thing::CoAP::Status::Created("Đã tắt quạt !");
      }
    } else {
      return Thing::CoAP::Status::Created("Đang bật chế độ auto!");
    }
  });
  server.CreateResource("RemChe", Thing::CoAP::ContentFormat::TextPlain, true)
  .OnGet([](Thing::CoAP::Request & request) {
    std::string result;
    if (pos == 140)
      result = "Rèm đang mở";
    else
      result = "Rèm đang đóng";
    return Thing::CoAP::Status::Content(result);
  }).OnPost([](Thing::CoAP::Request & request) {
    auto payload = request.GetPayload();
    std::string message(payload.begin(), payload.end());
    if (action == 1) {
      if (message == "on") {
        for (int i = pos; i <= SERVO_mo; i += 5) {
          servo_1.write(i);
          delay(100);
          pos = i;
        }
      } else if (message == "off") {
        for (int i = pos; i >= SERVO_dong; i -= 5) {
          servo_1.write(i);
          delay(100);
          pos = i;
        }
      } else {
        return Thing::CoAP::Status::BadRequest();
      }
      if (message == "on") {
        return Thing::CoAP::Status::Created("Đã mở rèm !");
      } else {
        return Thing::CoAP::Status::Created("Đã đóng rèm !");
      }
    } else {
      return Thing::CoAP::Status::Created("Đang bật chế độ auto!");
    }

  });
  server.CreateResource("isAutoMode", Thing::CoAP::ContentFormat::TextPlain, true)
  .OnGet([](Thing::CoAP::Request & request) {
    std::string result;
    if (action == 0)
      result = "Chế độ tự động";
    else
      result = "chế độ thủ công";
    return Thing::CoAP::Status::Content(result);
  }).OnPost([](Thing::CoAP::Request & request) {
    auto payload = request.GetPayload();
    std::string message(payload.begin(), payload.end());
    if (message == "yes") {
      action = 0;
    } else if (message == "no") {
      action = 1;
    } else {
      return Thing::CoAP::Status::BadRequest();
    }
    return Thing::CoAP::Status::Created("Done !");
  });
  
  server.Start();
}

void loop() {
  server.Process();
  // auto mode
  if (action == 0) {
    // đọc cảm biến đất
    for (int i = 0; i < 10; i++) {
      real_value += analogRead(A0);
    }
    value = real_value / 10;
    real_value = 0;
    int percent = map(value, 100, 1023, 0, 100);
    percent = 100 - percent;
    Serial.print(percent);
    Serial.print('%');
    Serial.print(F("Giá Trị Analog: "));
    Serial.print(value);
    Serial.println();
    // bật tắt auto khi độ ẩm đất < 50
    if (percent < 50) {
      digitalWrite(MAY_BOM, LOW);
    } else {
      digitalWrite(MAY_BOM, HIGH);
    }
    // đọc cảm biến ánh sáng
    float lux = lightMeter.readLightLevel();
    Serial.print(F("Light: "));
    Serial.print(lux);
    Serial.println(" lx");
    // cường độ ánh sáng <70 mở rèm
    if (lux < 70) {
      for (int i = pos; i <= SERVO_mo; i += 5) {
        servo_1.write(i);
        delay(100);
        pos = i;
      }
    } else {
      for (int i = pos; i >= SERVO_dong; i -= 5) {
        servo_1.write(i);
        delay(100);
        pos = i;
      }
    }

    // đọc độ ẩm, nhiệt độ không khí
    float h = dht.readHumidity();
    float t = dht.readTemperature();
    // reads failed and exit early (to try again).
    if (isnan(h) || isnan(t)) {
      Serial.println(F("Failed to read from DHT sensor!"));
    }
    Serial.print(F("Humidity: "));
    Serial.print(h);
    Serial.print(" %\t");
    Serial.print(F("Temperature: "));
    Serial.print(t);
    Serial.println(F(" *C \n"));
    // nhiệt độ >30  mở quạt
    if (t > 30 ) {
      digitalWrite(QUAT, LOW);
    } else {
      digitalWrite(QUAT, HIGH);
    }
    delay(5000);
  }
}
